
extends Node

const _texture_format = "res://sprites/numbers/64x64/Set_%d_%d.png";
var _number_sprites = []

func _ready():
	_load_number_sprites();

func _load_number_sprites():
	if _number_sprites.size() == 0:
		_number_sprites.append(null);
		for i in range(1,6):
			_number_sprites.append([]);
			for n in range(10):
				var texture = load(_texture_format % [i, n]);
				_number_sprites[i].append(texture);

func fetch_number(set, value):
	return _number_sprites[set][value];
