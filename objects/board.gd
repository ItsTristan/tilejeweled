
extends Node2D

export var cell_size = 64;
export var rows = 9;
export var cols = 6;
var _boundary = Rect2(0,0,cols*cell_size,rows*cell_size);

const IDLE = 0;
const SELECTING = 1;
const FIRING = 2;
const FLIPPING = 3;
const POPPING = 4;
const FALLING = 5;
const WAITING = 6;

var state = IDLE setget set_state;

var _board = [];
var _selected = [];
var _pop_queue = [];
var _col_stacks = [];
var timer = 0;

var combo = 0;
var chain = 0;
var counter = 0;
var iterations = 0;

var score = 0 setget _set_score, _get_score;
var moves = 20;

func _ready():
	set_process(true);
	set_process_input(true);
	# Init tiles
	var tile_res = load("res://objects/tile.tscn");
	for i in range(rows):
		_board.append([]);
		_col_stacks.append(0);
		for j in range(cols):
			var tile = tile_res.instance();
			_board[i].append(null);
			set_cell(i,j,tile);
			tile.set_pos(Vector2(j*cell_size,i*cell_size));
			add_child(tile);

func set_timer(time):
	if time > 0:
		timer = max(0.05, lerp(time,0.05, min((iterations-1)/12.0,1)));
	else:
		timer = 0;

func set_cell(i,j,tile):
	_board[i][j] = tile;
	tile.inform_position(i,j);

func _process(delta):
	if timer > 0:
		timer -= delta;
		if timer <= 0:
			trigger_timer();
	if state == FALLING or state == POPPING:
		fall(delta);

func _input(event):
	if event.is_action_pressed("select"):
		select(event.pos - self.get_global_pos());

func set_state(new_state):
	if state == new_state: return;
	state = new_state;
	debug("state = ", state);
	if state == IDLE:
		debug("combo = ", combo);
		debug("chain = ", chain);
		score += 50*combo*chain;
		moves += chain / 3;
		debug("moves = ", moves);
		iterations = 0;
	elif state == SELECTING:
		combo = 0;
		chain = 0;
	elif state == FIRING:
		fire();
	elif state == FLIPPING:
		flip();
	elif state == POPPING:
		pop();
	elif state == WAITING:
		if _selected.size() > 0:
			set_timer(0.3);
		else:
			set_state(FIRING);

func select(pos):
	if moves <= 0: return;
	if not state in [IDLE, SELECTING]: return;
	if _boundary.has_point(pos):
		set_state(SELECTING);
		var tile = _board[pos.y / cell_size][pos.x / cell_size];
		if not tile.selected:
			tile.selected = true;
			tile.value += 1;
			_selected.append(tile);
			timer = 1;
			get_node("SoundEffects").play("block_select");
			moves -= 1;

func trigger_timer():
	if state == SELECTING:
		set_state(FIRING);
	elif state == FIRING:
		pass;
	elif state == FLIPPING:
		flip();
	elif state == POPPING:
		pop();
	elif state == WAITING:
		set_state(FIRING);

func fire():
	iterations += 1;
	if _selected.size() > 0:
		counter = 0;
		chain -= 1;
		for tile in _selected:
			tile.selected = false;
			tile.chain = 1;
			if tile.value == 0:
				_pop_queue.append(tile);
		_selected = [];
		set_state(FLIPPING);
	else:
		set_state(IDLE);

func flip():
	var changed = false;
	for i in range(rows):
		for j in range(cols):
			var tile = _board[i][j];
			if tile.chain == 0:
				continue;
			if tile.chain == 1:
				_mark_flips(tile, i, j);
				tile.chain = 0;
				changed = true;
	for i in range(rows):
		for j in range(cols):
			var tile = _board[i][j];
			if tile.flip_color != 0:
				tile.flip(tile.flip_color);
				tile.flip_color = 0;
				if tile.value == 0:
					_pop_queue.append(tile);
			if tile.chain == 2:
				tile.chain = 1;
				changed = true;
	if not changed:
		set_state(POPPING);
	else:
		chain += 1;
		set_timer(0.3);

func _mark_flips(tile, row, col):
	if tile.value == 0: return;
	if tile.flip_color != 0: return;
	if 0 < row: _try_flip(tile, row-1, col);
	if row+1 < rows: _try_flip(tile, row+1, col);
	if 0 < col: _try_flip(tile, row, col-1);
	if col+1 < cols: _try_flip(tile, row, col+1);

func _try_flip(tile, row, col):
	var other = _board[row][col];
	if other.value == 0: return;
	if tile.value >= other.value and tile.color != other.color:
		get_node("FlipSoundEffects").play("flip", true);
		other.chain = 2;
		other.flip_color = tile.color;

func _try_select(row, col, source):
	var tile = _board[row][col];
	if source.color == tile.color and tile.value > 0:
		_selected.append(tile);
		tile.glow();
		tile.selected = true;
	
func pop():
	if _pop_queue.size() > 0:
		combo += 1;
		counter += 1;
		score += 25*counter;
		var tile = _pop_queue[0];
		var row = tile.row;
		var col = tile.col;
		_pop_queue.pop_front()
		tile.pop();
		get_node("SoundEffects").play("block_break_%d" % (min(counter+combo/10,10)));
		debug(counter);
		
		if 0 < row: _try_select(row-1,col,tile);
		if row+1 < rows: _try_select(row+1,col,tile);
		if 0 < col: _try_select(row,col-1,tile);
		if col+1 < cols: _try_select(row,col+1,tile);
		
		for i in range(row):
			var k = row - i;
			var t = _board[k-1][col];
			set_cell(k, col, t);
			t.motion += cell_size;
		set_cell(0, col, tile);
		_col_stacks[col] += 1;
		tile.motion = cell_size*_col_stacks[col];
		tile.set_pos(Vector2(col*cell_size,-_col_stacks[col]*cell_size));
		set_timer(0.1);
	else:
		set_state(FALLING);

func fall(delta):
	var changed = false;
	for i in range(rows):
		for j in range(cols):
			var tile = _board[i][j];
			if tile.motion == 0: continue;
			changed = true;
			var amount = delta*min(24, lerp(4,24, min((iterations-1)/12.0,1)))*cell_size;
			tile.motion -= amount;
			tile.set_pos(tile.get_pos()+Vector2(0,amount));
			if tile.motion < 0:
				tile.set_pos(tile.get_pos()-Vector2(0,-tile.motion));
				tile.motion = 0;
	if not changed:
		for c in range(_col_stacks.size()):
			_col_stacks[c] = 0;
		if state == FALLING:
			set_state(WAITING);

func _get_score():
	return score;

func _set_score(new_score):
	pass;

func debug(a=null,b=null,c=null,d=null,e=null,f=null,g=null,h=null,i=null,j=null,k=null,l=null):
	if OS.is_debug_build():
		print(a,b,c,d,e,f,g,h,i,j,k,l);