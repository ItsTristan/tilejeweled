
extends Node2D

const YELLOW = 1;
const ORANGE = 2;
const GREEN = 3;
const BLUE = 4;
const RED = 5;
const COLORS = [YELLOW, ORANGE, GREEN, BLUE, RED];

export var selected = false setget _select;

var chain = 0;
var color = null setget _change_color;
var value = null setget _change_value;
var _texture;
var _timer = 0;

var motion = 0;
var flip_color = false;

var row;
var col;

func _ready():
	reset();

func reset():
	randomize();
	change_value(randi()%4+1, COLORS[randi()%COLORS.size()]);
	_update();
	_select(false);
	chain = 0;

func change_value(new_value, new_color=null):
	if new_color != null:
		color = new_color;
	new_value = clamp(new_value, 0, 10) % 10;
	if new_value != value:
		value = new_value;
		set_process(true);
		_timer = 0.25;
		get_node("ChangeValueEffect").show();
	_update()

func _change_value(new_value):
	change_value(new_value, color);

func _process(delta):
	_timer -= delta;
	if _timer <= 0:
		set_process(false);
		get_node("ChangeValueEffect").hide();
	else:
		get_node("ChangeValueEffect").set_opacity(4*_timer);

func _change_color(new_color):
	change_value(value, new_color);

func _update():
	_texture = get_node("/root/resources").fetch_number(color, value);
	get_node("Sprite").set_texture(_texture);

func _select(new_select_value):
	selected = new_select_value;
	var effect = get_node("SelectedEffect");
	if effect != null:
		effect.set_hidden(not selected);

func flip(color):
	if value > 0:
		change_value(value-1, color);

func inform_position(new_row, new_col):
	row = new_row;
	col = new_col;

func pop():
	reset();

func glow():
	pass;