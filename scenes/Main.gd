extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var board;

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true);
	board = get_node("Board");

func _process(delta):
	get_node("Panel/Score").set_text("Score: %d" % board.score);
	get_node("Panel/Combo").set_text("Combo: %d" % board.combo);
	get_node("Panel/Chain").set_text("Chain: %d" % board.chain);
	get_node("Panel/MovesLeft").set_text("Moves Left: %d" % board.moves);
	
	if board.moves == 0 and board.state == board.IDLE:
		print("Game Over!");
		get_node("Panel/MovesLeft").set_text("Game Over!");
		set_process(false);